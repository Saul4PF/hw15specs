 expect(isShowingLoader()).toBe(true);
    });

    describe("during loading", () => {
      beforeEach(() => {
        findButton().dispatchEvent(new Event("click"));
      })
    })

    it("ignores button clicks", () => {
      expect(jest.getTimerCount()).toBe(1);
    })

    describe("post load", () => {
      beforeEach(() => {
       
      });

      it("shows name", () => {
        expect(Math.random).toHaveBeenCalled();
        expect(findName().textContent).toContain("saul");
      });

      it("shows name by itself", () => {
        expect(findName().textContent).toBe("saul;");
      });

      describe("when button is pressed again", () => {
        beforeEach(() => {
          findButton().dispatchEvent(new Event("click"));
        });

      it("replaces name with loader", () => {
        expect(findName().textContent).not.toContain("saul");
        expect(isShowingLoader()).toBe(true);
        });
      });
    });
  });
});
